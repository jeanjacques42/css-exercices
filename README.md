# css-exercices

## Exercice 1 - Selecteurs (exercice1.html)

Commencer par créer un fichier css et le linker au html
1. Souligner les h1
2. Mettre le texte des h2 des sections en rouge
3. Mettre les éléments ayant la classe bold en gras
4. Changer la taille du texte de l'élément avec l'id second
5. Mettre les h2 de l'élément avec l'id second en italique
6. Mettre une bordure noire sur les élément avec la class myClass
7. Mettre la couleur de la bordure en vert pour les éléments de type article avec la classe myClass
8. Mettre une couleur de fond gris claire aux span
9. Mettre en majuscule les span à l'intérieur de p
10. Mettre une marge de 20px aux p directement à l'intérieur de section
11. Indenter de 15px les h1 et les h2 directement dans un élément avec l'id second
12. Mettre un padding de 10px aux paragraphes ayant la classe bold